﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Aldeano : MonoBehaviour {

    public enum Clase {

        pueblo,
        ejercito,
        religion,
        comercio

    }

    [Serializable]
    public class ModificadorRecurso {

        public TipoRecurso tipoRecurso;
        public int modificadorRecurso;

        public ModificadorRecurso(TipoRecurso tr, int modificador) {
            this.tipoRecurso = tr;
            this.modificadorRecurso = modificador;
        }

    }

    public string nombre;

    public Clase clase;
    public ModificadorRecurso modificadorOro = new ModificadorRecurso(TipoRecurso.oro, 0);
    public ModificadorRecurso modificadorCristianismo = new ModificadorRecurso(TipoRecurso.cristianismo, 0);
    public ModificadorRecurso modificadorEjercito = new ModificadorRecurso(TipoRecurso.ejercito, 0);

    public ModificadorRecurso getModificador(TipoRecurso tipoRecurso) {

        switch (tipoRecurso) {
            case TipoRecurso.oro:
                return modificadorOro;
            case TipoRecurso.cristianismo:
                return modificadorCristianismo;
            case TipoRecurso.ejercito:
                return modificadorEjercito;
            default:
                return null;
        }


    }

    private int anosVividos;

    [Range(0, 100)]
    public int ratioAparicion;

    public Aldeano(Aldeano ald) {

        this.nombre = ald.nombre;
        this.modificadorOro = ald.modificadorOro;
        this.modificadorCristianismo = ald.modificadorCristianismo;
        this.modificadorEjercito = ald.modificadorEjercito;
        this.ratioAparicion = ald.ratioAparicion;
        this.clase = ald.clase;

    }

    public void addAnosVividos(int anos) {
        this.anosVividos += anos;
    }

    public int getAnosVividos() {

        return this.anosVividos;

    }

}
