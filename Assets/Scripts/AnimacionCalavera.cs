﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimacionCalavera : MonoBehaviour {

    public void setNumero(int numero) {

        this.GetComponentInChildren<Text>().text = numero.ToString();

    }

    public void OnDisable() {
        this.gameObject.SetActive(false);
    }

}
