﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimarTexto : MonoBehaviour {

    public AudioSource sonidoEscrituraMano;


    public void animarTexto(Text cuadroDeTexto, string texto, float duracionAnimacion) {
        StartCoroutine(animarTextoCoroutine(cuadroDeTexto, texto, 0f, duracionAnimacion));
    }

    public void animarTexto(Text cuadroDeTexto, string texto, float delayInicial, float duracionAnimacion) {
        StartCoroutine(animarTextoCoroutine(cuadroDeTexto, texto, delayInicial, duracionAnimacion));
    }


    IEnumerator animarTextoCoroutine(Text cuadroDeTexto, string texto, float delayInicial, float duracionAnimacion) {

        var duracionActualAnimacion = 0f;

        while (duracionActualAnimacion <= delayInicial) {

            duracionActualAnimacion += Time.deltaTime;
            yield return null;

        }

        sonidoEscrituraMano.Play();

        duracionActualAnimacion = 0f;

        while (duracionActualAnimacion <= duracionAnimacion) {

            var t = texto.Substring(0, Mathf.RoundToInt((duracionActualAnimacion / duracionAnimacion) * (texto.Length)));

            cuadroDeTexto.text = t;

            duracionActualAnimacion += Time.deltaTime;
            yield return null;

        }

        sonidoEscrituraMano.Stop();
        cuadroDeTexto.text = texto;
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
