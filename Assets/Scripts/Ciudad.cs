﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Ciudad : MonoBehaviour {

    [Range(0, 100)]
    public int oro;
    [Range(0, 100)]
    public int cristianismo;
    [Range(0, 100)]
    public int ejercito;

    public int valorParaSonidoDeAlerta = 20;

    public int poblacionMaxima;
    public int poblacionActual;
    public int esperanzaDeVida;

    public float multiplicadorEnvejecimiento;
    public Aldeano.Clase[] clasesSobreLasQueAplicaMultiplicador;

    public ModificadorZona[] modificadoresDeZona;

    public int perdidaPorAnoOro;
    public int perdidaPorAnoCristianismo;
    public int perdidaPorAnoEjercito;

    public List<Aldeano> aldeanos;

    public Medidor medidorOro;
    public Medidor medidorCristianismo;
    public Medidor medidorEjercito;
    public Text medidorPoblacion;

    public AudioSource sonidoMuerte;
    public AudioSource sonidoHover;
    public AudioSource sonidoNegacion;
    public GameObject imagenMuerte;
    public GameObject efectosAldeanoMuerto;
    public GameObject efectoAñadirAldeano;

    [HideInInspector]
    public bool alive = true;

    private float tiempoAnimacionMedidores = 1f;

    private bool oroInAlerta;
    private bool cristianismoInAlerta;
    private bool ejercitoInAlerta;

    public enum TipoModificadorZona {
        todos,
        oro,
        cristianismo,
        ejercito
    }

    [Serializable]
    public class ModificadorZona {
        public TipoModificadorZona tipoModificador;
        public Aldeano.Clase[] clasesDondeAplica;
        public int modificador;


        public TipoRecurso getTipoRecursoPorTipoModificador(TipoModificadorZona tipoModificador) {

            switch (tipoModificador) {
                case TipoModificadorZona.todos:
                    return TipoRecurso.oro;
                case TipoModificadorZona.oro:
                    return TipoRecurso.oro;
                case TipoModificadorZona.cristianismo:
                    return TipoRecurso.cristianismo;
                case TipoModificadorZona.ejercito:
                    return TipoRecurso.ejercito;
            }
            return TipoRecurso.oro;
        }

        public bool isAldeanoInClasesDondeAplica(Aldeano.Clase claseAldeano) {

            foreach (var c in clasesDondeAplica) {
                if (c == claseAldeano) {
                    return true;
                }
            }

            return false;

        }

    }

    void Start() {

        alive = true;

        this.poblacionActual = aldeanos.Count;

        this.medidorOro.setValor(this.oro / 100f, tiempoAnimacionMedidores);
        this.medidorCristianismo.setValor(this.cristianismo / 100f, tiempoAnimacionMedidores);
        this.medidorEjercito.setValor(this.ejercito / 100f, tiempoAnimacionMedidores);

        setTextoPoblacion(poblacionActual);

    }

    public bool sePuedeAñadirAldeano() {
        if (this.poblacionActual >= poblacionMaxima) {
            return false;
        }

        return alive;
    }

    public bool añadirAldeano(Aldeano aldeano) {

        if (alive) {
            if (this.poblacionActual >= poblacionMaxima) {
                return false;
            }

            this.aldeanos.Add(aldeano);
            efectoAñadirAldeano.SetActive(true);
            efectoAñadirAldeano.GetComponent<AnimacionCalavera>().setNumero(1);
            Debug.Log("Añadido aldeano " + aldeano.nombre + " a " + this.gameObject.name);

            this.poblacionActual = aldeanos.Count;
            setTextoPoblacion(poblacionActual);

            return true;
        }

        return false;

    }

    public void actualizarEstado() {

        if (alive) {

            foreach (var aldeano in aldeanos) {

                this.oro += aldeano.modificadorOro.modificadorRecurso;
                this.cristianismo += aldeano.modificadorCristianismo.modificadorRecurso;
                this.ejercito += aldeano.modificadorEjercito.modificadorRecurso;

                foreach (var mod in modificadoresDeZona) {

                    if (mod.isAldeanoInClasesDondeAplica(aldeano.clase)) {

                        if (mod.tipoModificador == TipoModificadorZona.todos) {

                            this.oro += mod.modificador;
                            this.cristianismo += mod.modificador;
                            this.ejercito += mod.modificador;

                        }
                        else {

                            switch (mod.getTipoRecursoPorTipoModificador(mod.tipoModificador)) {
                                case TipoRecurso.oro:
                                    this.oro += mod.modificador;
                                    break;
                                case TipoRecurso.cristianismo:
                                    this.cristianismo += mod.modificador;
                                    break;
                                case TipoRecurso.ejercito:
                                    this.ejercito += mod.modificador;
                                    break;
                                default:
                                    break;

                            }
                        }
                    }
                }
            }

            if (poblacionActual <= 0) {
                this.oro -= perdidaPorAnoOro;
                this.cristianismo -= perdidaPorAnoCristianismo;
                this.ejercito -= perdidaPorAnoEjercito;
            }

            this.medidorOro.setValor(this.oro / 100f, tiempoAnimacionMedidores);
            this.medidorCristianismo.setValor(this.cristianismo / 100f, tiempoAnimacionMedidores);
            this.medidorEjercito.setValor(this.ejercito / 100f, tiempoAnimacionMedidores);

            this.comprobarAldeanos();

            if (this.oro <= valorParaSonidoDeAlerta && !oroInAlerta) {

                oroInAlerta = true;
                medidorOro.activarAlerta();

            }
            else if (this.oro >= valorParaSonidoDeAlerta && oroInAlerta) {

                oroInAlerta = false;
                medidorOro.desactivarAlerta();

            }

            if (this.cristianismo <= valorParaSonidoDeAlerta && !cristianismoInAlerta) {

                cristianismoInAlerta = true;
                medidorCristianismo.activarAlerta();

            }
            else if (this.cristianismo >= valorParaSonidoDeAlerta && cristianismoInAlerta) {

                cristianismoInAlerta = false;
                medidorCristianismo.desactivarAlerta();

            }

            if (this.ejercito <= valorParaSonidoDeAlerta && !ejercitoInAlerta) {

                ejercitoInAlerta = true;
                medidorEjercito.activarAlerta();

            }
            else if (this.ejercito >= valorParaSonidoDeAlerta && ejercitoInAlerta) {

                ejercitoInAlerta = false;
                medidorEjercito.desactivarAlerta();

            }

            if (this.oro <= 0 || this.cristianismo <= 0 || this.ejercito <= 0) {
                alive = false;
                this.die();
            }
        }
    }

    public void comprobarAldeanos() {

        if (alive) {
            List<Aldeano> aldeanosMuertos = new List<Aldeano>();

            foreach (var aldeano in aldeanos) {


                bool aplicaMultiplicador = false;
                foreach (var clase in clasesSobreLasQueAplicaMultiplicador) {
                    if (clase == aldeano.clase) {
                        aplicaMultiplicador = true;
                    }
                }

                if (aplicaMultiplicador) {
                    aldeano.addAnosVividos((int)Mathf.Round(1f * multiplicadorEnvejecimiento));
                }
                else {
                    aldeano.addAnosVividos(1);
                }


                if (aldeano.getAnosVividos() >= esperanzaDeVida) {
                    aldeanosMuertos.Add(aldeano);
                    Debug.Log("Muerto aldeano " + aldeano.nombre + " en " + this.gameObject.name);
                }
            }


            if (aldeanosMuertos.Count > 0) {
                efectosAldeanoMuerto.SetActive(true);
                efectosAldeanoMuerto.GetComponent<AnimacionCalavera>().setNumero(aldeanosMuertos.Count);
            }

            foreach (var ald in aldeanosMuertos) {
                aldeanos.Remove(ald);
            }


            this.poblacionActual = aldeanos.Count;
            setTextoPoblacion(poblacionActual);

        }
    }


    private void setTextoPoblacion(int valorPoblacion) {

        this.medidorPoblacion.text = string.Concat(valorPoblacion, " / ", poblacionMaxima);

    }

    public void die() {

        medidorPoblacion.gameObject.SetActive(false);
        medidorOro.gameObject.SetActive(false);
        medidorCristianismo.gameObject.SetActive(false);
        medidorEjercito.gameObject.SetActive(false);
        this.GetComponent<Animator>().SetTrigger("explosion");
        Debug.LogWarning("Ciudad " + this.gameObject.name + "  ha precido.");
        GameObject.FindObjectOfType<Controlador>().ciudadMuerta();
        sonidoMuerte.Play();

    }

    public void dieFinished1() {
        imagenMuerte.SetActive(true);
    }

    public void dieFinished2() {
        imagenMuerte.SetActive(true);
        this.GetComponent<Image>().enabled = false;
    }

    public void OnMouseEnter() {
        sonidoHover.Play();
        this.GetComponent<Animator>().SetTrigger("agrandar");
    }

    public void OnMouseExit() {
        this.GetComponent<Animator>().SetTrigger("encoger");
    }

}
