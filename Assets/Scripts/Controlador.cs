﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Controlador : MonoBehaviour {

    public float segundosPorAno;

    public GameObject introCanvas;
    public GameObject gameOverCanvas;

    public Image imagenAldeano;

    public Ciudad ciudadNorte;
    public Ciudad ciudadSur;
    public Ciudad ciudadEste;
    public Ciudad ciudadOeste;

    public Button botonCiudadNorte;
    public Button botonCiudadSur;
    public Button botonCiudadEste;
    public Button botonCiudadOeste;

    public Text calendarioDia;
    public Text calendarioMes;
    public Text calendarioAño;

    private bool controlesDehabilitados = false;


    private float segundosUltimoAno = 0f;
    private List<Ciudad> ciudades = new List<Ciudad>();

    public Text textoDescripcion;
    public GestorTextos gestorTexto;
    private int aldenaosSpawneados = 0;

    public GameObject aldeanos;
    private List<Aldeano> tiposAldeano = new List<Aldeano>();
    private int probabilidadTotalAldeanos;

    private int añoActual = 0;
    private GameObject aldeanoActual;
    private bool preguntasDesabilitadas = false;
    private bool iniciado = false;
    private bool gameOver = false;

    public AudioSource sonidoGritoAldeano;
    public AudioSource sonidoMovimientoAldeano;
    public AudioSource sonidoAbsorberAldeano;
    public AudioSource mainTheme;

    // Use this for initialization
    void Start() {

        introCanvas.SetActive(true);
        var text = introCanvas.GetComponentInChildren<Text>();
        gestorTexto.animadorTexto.animarTexto(text, text.text, 4f);

        ciudades.Add(ciudadNorte);
        ciudades.Add(ciudadSur);
        ciudades.Add(ciudadEste);
        ciudades.Add(ciudadOeste);

        tiposAldeano.AddRange(aldeanos.GetComponentsInChildren<Aldeano>());

        foreach (var ald in tiposAldeano) {
            probabilidadTotalAldeanos += ald.ratioAparicion;
        }

    }


    private System.DateTime fechaCalendario = new System.DateTime(1498, 11, 17);
    void Update() {

        if (iniciado) {

            segundosUltimoAno += Time.deltaTime;

            fechaCalendario = fechaCalendario.AddMinutes(((365f * 24f * 60f) / segundosPorAno) * Time.deltaTime);

            if (segundosUltimoAno >= segundosPorAno) {

                añoActual++;
                segundosUltimoAno = 0f;

                foreach (var ciudad in ciudades) {
                    ciudad.actualizarEstado();
                }

                fechaCalendario = new System.DateTime(1498 + añoActual, 11, 17);
            }

            calendarioDia.text = fechaCalendario.Day.ToString();
            calendarioMes.text = fechaCalendario.Month.ToString();
            calendarioAño.text = fechaCalendario.Year.ToString();

        }
        else {

            if (gameOver) {

                if (isInGameOver && (Input.GetMouseButtonUp(0) || Input.GetKeyUp(KeyCode.R))) {
                    SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
                }

            }
            else {

                if (!isInActivacion && Input.GetMouseButtonUp(0)) {
                    StartCoroutine(activarConDelayCoroutine(0.25f));
                    introCanvas.GetComponent<Animator>().SetTrigger("iniciar");
                }

            }
        }

    }


    public void preguntaPulsada(TipoRecurso tipoPregunta) {

        if (!preguntasDesabilitadas) {
            gestorTexto.actualizarTextosDespuesDePregunta(tipoPregunta, aldeanoActual.GetComponent<Aldeano>());
            StartCoroutine(deshabilitarControles(gestorTexto.duracionAnimacionDialogo));
        }
        preguntasDesabilitadas = true;

    }

    public void preguntaPulsada(int pregunta) {

        TipoRecurso tipoPregunta;

        switch (pregunta) {
            case 1:
                tipoPregunta = TipoRecurso.oro;
                preguntaPulsada(tipoPregunta);
                break;

            case 2:
                tipoPregunta = TipoRecurso.cristianismo;
                preguntaPulsada(tipoPregunta);
                break;

            case 3:
                tipoPregunta = TipoRecurso.ejercito;
                preguntaPulsada(tipoPregunta);
                break;

            default:
                break;
        }

    }


    public void añadirAldeano(Ciudad ciudad) {

        if (ciudad.sePuedeAñadirAldeano()) {
            StartCoroutine(añadirAldeanoCoroutine(ciudad));
        }
        else {
            ciudad.GetComponent<Animator>().SetTrigger("lleno");
            ciudad.sonidoNegacion.Play();
        }

    }

    public GameObject generarAldeano() {

        //BORRAR
        aldenaosSpawneados++;


        int index = 0;
        int lastIndex = probabilidadTotalAldeanos - 1;
        int sumaProb = probabilidadTotalAldeanos;

        while (index < tiposAldeano.Count) {
            // Do a probability check with a likelihood of weights[index] / weightSum.
            if (Random.Range(0, sumaProb) < tiposAldeano[index].ratioAparicion) {
                return tiposAldeano[index].gameObject;
            }

            // Remove the last item from the sum of total untested weights and try again.
            sumaProb -= tiposAldeano[index++].ratioAparicion;
        }

        // No other item was selected, so return very last index.
        return tiposAldeano[index].gameObject;

    }

    private int ciudadesMuertas = 0;
    public void ciudadMuerta() {

        ciudadesMuertas++;

        if (ciudadesMuertas >= 3) {

            iniciado = false;

            gameOverCanvas.SetActive(true);
            StartCoroutine(gameOverConDelay(0.75f));

        }

    }

    private bool isInActivacion = false;
    IEnumerator activarConDelayCoroutine(float delay) {

        float t = 0f;
        isInActivacion = true;

        aldeanoActual = Instantiate(generarAldeano());
        imagenAldeano.sprite = aldeanoActual.GetComponent<SpriteRenderer>().sprite;

        while (t < delay) {
            t += Time.deltaTime;
            yield return null;
        }

        isInActivacion = false;
        iniciado = true;
        introCanvas.SetActive(false);
        mainTheme.gameObject.SetActive(true);

        gestorTexto.actualizarTextosNuevoAldeano();
        StartCoroutine(deshabilitarControles(gestorTexto.duracionAnimacionDialogo));

    }

    IEnumerator añadirAldeanoCoroutine(Ciudad ciudad) {

        var imageAnimator = imagenAldeano.GetComponent<Animator>();
        StartCoroutine(deshabilitarControles(gestorTexto.duracionAnimacionDialogo));

        sonidoGritoAldeano.Play();
        sonidoMovimientoAldeano.Play();
        imageAnimator.SetTrigger("salir");

        yield return new WaitForSeconds(0.5f);

        Destroy(aldeanoActual);

        gestorTexto.actualizarTextosNuevoAldeano();

        aldeanoActual = Instantiate(generarAldeano());

        ciudad.añadirAldeano(aldeanoActual.GetComponent<Aldeano>());
        imagenAldeano.sprite = aldeanoActual.GetComponent<SpriteRenderer>().sprite;
        sonidoAbsorberAldeano.Play();
        imageAnimator.SetTrigger("entrar");

        preguntasDesabilitadas = false;

    }

    private bool isInGameOver = false;
    IEnumerator gameOverConDelay(float delay) {

        float t = 0f;

        while (t < delay) {
            t += Time.deltaTime;
            yield return null;
        }

        isInGameOver = true;
        gameOver = true;

    }

    IEnumerator deshabilitarControles(float tiempo) {

        var tiempoActual = 0f;
        controlesDehabilitados = true;

        while (tiempoActual <= tiempo) {

            botonCiudadNorte.enabled = false;
            botonCiudadEste.enabled = false;
            botonCiudadOeste.enabled = false;
            botonCiudadSur.enabled = false;

            gestorTexto.botonPregunta1.enabled = false;
            gestorTexto.botonPregunta2.enabled = false;
            gestorTexto.botonPregunta3.enabled = false;

            tiempoActual += Time.deltaTime;
            yield return null;
        }

        botonCiudadNorte.enabled = true;
        botonCiudadEste.enabled = true;
        botonCiudadOeste.enabled = true;
        botonCiudadSur.enabled = true;

        gestorTexto.botonPregunta1.enabled = true;
        gestorTexto.botonPregunta2.enabled = true;
        gestorTexto.botonPregunta3.enabled = true;

        controlesDehabilitados = false;

    }

}
