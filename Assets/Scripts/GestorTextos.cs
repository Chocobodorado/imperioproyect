﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class GestorTextos : MonoBehaviour {

    [Serializable]
    public class Pregunta {
        public TipoRecurso tipoPregunta;
        [HideInInspector]
        public int ultimaPregunta = 0;
        public string[] preguntas;
    }

    [Serializable]
    public class Respuesta {

        public TipoRecurso tipoRespuesta;
        public int valorMinimo;
        public int valorMaximo;
        public string nombreAldeano = "";
        [HideInInspector]
        public int ultimaRespuesta = 0;
        public string[] respuestas;

    }

    public string[] saludos;
    private int ultimoSaludo;
    public Pregunta[] preguntas;
    public Respuesta[] respuestas;

    public Text textoDialogo;
    public Button botonPregunta1;
    public Text textoPregunta1;
    public Button botonPregunta2;
    public Text textoPregunta2;
    public Button botonPregunta3;
    public Text textoPregunta3;

    public AnimarTexto animadorTexto;

    public float duracionAnimacionDialogo;
    public float duracionAnimacionBotones;

    void Start() {

        shuffle(saludos);

        foreach (var pregunta in preguntas) {
            shuffle(pregunta.preguntas);
        }

        foreach (var respuesta in respuestas) {
            shuffle(respuesta.respuestas);
        }

    }

    public void actualizarTextosNuevoAldeano() {

        botonPregunta1.interactable = true;
        botonPregunta2.interactable = true;
        botonPregunta3.interactable = true;

        animadorTexto.animarTexto(textoDialogo, getRandomSaludo(), duracionAnimacionDialogo);

        animadorTexto.animarTexto(textoPregunta1, getRandomPregunta(TipoRecurso.oro), duracionAnimacionDialogo, duracionAnimacionBotones);
        animadorTexto.animarTexto(textoPregunta2, getRandomPregunta(TipoRecurso.cristianismo), duracionAnimacionDialogo, duracionAnimacionBotones);
        animadorTexto.animarTexto(textoPregunta3, getRandomPregunta(TipoRecurso.ejercito), duracionAnimacionDialogo, duracionAnimacionBotones);

    }

    public void actualizarTextosDespuesDePregunta(TipoRecurso tipoPregunta, Aldeano aldeano) {

        animadorTexto.animarTexto(textoDialogo, getRandomRespuesta(tipoPregunta, aldeano), duracionAnimacionDialogo);

        switch (tipoPregunta) {
            case TipoRecurso.oro:
                botonPregunta2.interactable = false;
                botonPregunta3.interactable = false;
                break;
            case TipoRecurso.cristianismo:
                botonPregunta1.interactable = false;
                botonPregunta3.interactable = false;
                break;
            case TipoRecurso.ejercito:
                botonPregunta1.interactable = false;
                botonPregunta2.interactable = false;
                break;
            default:
                break;
        }

    }

    public string getRandomSaludo() {

        if (ultimoSaludo >= saludos.Length) {
            shuffle(saludos);
            ultimoSaludo = 0;
        }

        ultimoSaludo++;
        return saludos[ultimoSaludo - 1];

    }

    public string getRandomPregunta(TipoRecurso tipoPregunta) {

        Pregunta pregunta = null;

        foreach (var p in preguntas) {
            if (p.tipoPregunta == tipoPregunta) {
                pregunta = p;
            }
        }

        if (pregunta.ultimaPregunta >= pregunta.preguntas.Length) {
            shuffle(pregunta.preguntas);
            pregunta.ultimaPregunta = 0;
        }

        pregunta.ultimaPregunta++;
        return pregunta.preguntas[pregunta.ultimaPregunta - 1];

    }

    public string getRandomRespuesta(TipoRecurso tipoRespuesta, Aldeano aldeano) {

        Respuesta respuesta = null;

        foreach (var r in respuestas) {
            if (r.tipoRespuesta == tipoRespuesta &&
                ((r.nombreAldeano.Equals(aldeano.nombre, StringComparison.InvariantCultureIgnoreCase)) ||
                (r.valorMinimo <= aldeano.getModificador(tipoRespuesta).modificadorRecurso && r.valorMaximo >= aldeano.getModificador(tipoRespuesta).modificadorRecurso))
                ) {

                respuesta = r;

            }
        }

        if (respuesta == null) { return ""; }


        if (respuesta.ultimaRespuesta >= respuesta.respuestas.Length) {
            shuffle(respuesta.respuestas);
            respuesta.ultimaRespuesta = 0;
        }

        respuesta.ultimaRespuesta++;
        return respuesta.respuestas[respuesta.ultimaRespuesta - 1];

    }

    public void shuffle(string[] lista) {

        var temp = "";
        for (int i = 0; i < lista.Length; i++) {
            int rnd = UnityEngine.Random.Range(0, lista.Length);
            temp = lista[rnd];
            lista[rnd] = lista[i];
            lista[i] = temp;
        }
    }


}
