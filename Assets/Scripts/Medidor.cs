﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Medidor : MonoBehaviour {


    public Image radialRojo;
    public Image radialVerde;
    public Image radialBlanco;


    public void activarAlerta() {
        this.GetComponent<Animator>().SetTrigger("medidorBajo");
    }

    public void desactivarAlerta() {
        this.GetComponent<Animator>().SetTrigger("medidorAlto");
    }

    public void setValor(float valor, float tiempoAnimacion) {

        StopAllCoroutines();
        StartCoroutine(rellenarBarraCoroutine(tiempoAnimacion, valor));

    }

    IEnumerator rellenarBarraCoroutine(float tiempo, float valorAlcanzar) {

        var diferencia = radialBlanco.fillAmount - valorAlcanzar;
        float tActual = 0f;

        if (diferencia <= 0f) {

            radialVerde.fillAmount = valorAlcanzar;
            radialRojo.fillAmount = valorAlcanzar;

            //Llenando
            while (tActual < tiempo) {

                radialBlanco.fillAmount -= (diferencia / tiempo) * Time.deltaTime;
                tActual += Time.deltaTime;

                yield return null;
            }

            radialBlanco.fillAmount = valorAlcanzar;

        }
        else {

            radialBlanco.fillAmount = valorAlcanzar;
            radialVerde.fillAmount = valorAlcanzar;

            //Vaciando
            while (tActual < tiempo) {

                radialRojo.fillAmount -= (diferencia / tiempo) * Time.deltaTime;
                tActual += Time.deltaTime;

                yield return null;
            }

            radialRojo.fillAmount = valorAlcanzar;

        }

    }

}
